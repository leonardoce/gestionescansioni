unit GestioneConfigurazione;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, IniFiles;

procedure CaricaInfo;
procedure SalvaInfo;

procedure ImpostaCartellaCorrente(Cartella:String);
function DammiCartellaCorrente:String;

implementation

const
  NOME_FILE_INI = 'configurazione.ini';

var
  CartellaImmagini:String = '.';

procedure CaricaInfo;
var
  Oggetto: TIniFile;
begin
  if FileExists(NOME_FILE_INI) then
  begin
    Oggetto := TIniFile.Create(NOME_FILE_INI);
    try
      CartellaImmagini := Oggetto.ReadString('Output', 'NomeCartella', '.');
    finally
      FreeAndNil(Oggetto);
    end;
  end;
end;

procedure SalvaInfo;
var
  Oggetto: TIniFile;
begin
  Oggetto := TIniFile.Create(NOME_FILE_INI);
  try
    Oggetto.WriteString('Output', 'NomeCartella', CartellaImmagini);
  finally
    FreeAndNil(Oggetto);
  end;
end;

function DammiCartellaCorrente:String;
begin
  Result := CartellaImmagini;
end;

procedure ImpostaCartellaCorrente(Cartella:String);
begin
  CartellaImmagini:=Cartella;
end;

end.

