unit form_scansioni;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  DelphiTwain, DelphiTwain_VCL, GestioneConfigurazione;

type

  { TForm1 }

  TForm1 = class(TForm)
    BSelezionaCartella: TButton;
    BScansione: TButton;
    ECartellaImmagini: TEdit;
    Label1: TLabel;
    SelectDirectoryDialog: TSelectDirectoryDialog;
    procedure BScansioneClick(Sender: TObject);
    procedure BSelezionaCartellaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
    Twain: TDelphiTwain;
    NomeFileLibero: String;

    procedure TwainTwainAcquire(Sender: TObject; const {%H-}Index: Integer;
      Image: TBitmap; var Cancel: Boolean);

    procedure CercaNomeFileLibero;
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.BSelezionaCartellaClick(Sender: TObject);
begin
  if SelectDirectoryDialog.Execute then
  begin
    ECartellaImmagini.Text:=SelectDirectoryDialog.FileName;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  GestioneConfigurazione.CaricaInfo;
  ECartellaImmagini.Text := GestioneConfigurazione.DammiCartellaCorrente;
end;

procedure TForm1.BScansioneClick(Sender: TObject);
var
  ExitForFailure:Boolean;
begin
  ExitForFailure := False;

  { Intanto salviamo la cartella dove fare
    la scansione delle immagini }
  GestioneConfigurazione.ImpostaCartellaCorrente(ECartellaImmagini.Text);
  GestioneConfigurazione.SalvaInfo;

  { Adesso cerco il nome di un file dove posso fare i
    miei giochi }
  CercaNomeFileLibero;
  if Length(NomeFileLibero)=0 then
  begin
    ShowMessage('Non c''e'' piu'' posto nella cartella scelta. Bisogna cambiare cartella!');
    ExitForFailure := True;
  end;

  if (not ExitForFailure) and (Twain=nil) then
  begin
    Twain := TDelphiTwain.Create;
    Twain.OnTwainAcquire := @TwainTwainAcquire;
  end;

  if (not ExitForFailure) and (not Twain.LoadLibrary) then
  begin
    ShowMessage('Driver dello scanner non installato');
    ExitForFailure := True;
  end;

  if not ExitForFailure then
  begin
    //Load source manager
    Twain.SourceManagerLoaded := TRUE;

    //Allow user to select source -> only the first time
    if not Assigned(Twain.SelectedSource) then
      Twain.SelectSource;

    if Assigned(Twain.SelectedSource) then begin
      //Load source, select transference method and enable (display interface)}
      Twain.SelectedSource.Loaded := TRUE;
      Twain.SelectedSource.ShowUI := TRUE;//display interface
      Twain.SelectedSource.Enabled := True;
    end;

  end;
end;

procedure TForm1.TwainTwainAcquire(Sender: TObject; const Index: Integer;
  Image: TBitmap; var Cancel: Boolean);
var
  ImmagineCompressa:TJPEGImage;
begin
  // Adesso salvo come JPEG l'immagine che mi e' arrivata
  ImmagineCompressa := TJPEGImage.Create;
  try
    ImmagineCompressa.Assign(Image);
    ImmagineCompressa.SaveToFile(NomeFileLibero);
    ShowMessage('Ho appena salvato il file '+NomeFileLibero);
  finally
    FreeAndNil(ImmagineCompressa);
  end;
  Cancel := True;//Only want one image
end;

procedure TForm1.CercaNomeFileLibero;
  function Suffisso(n:Integer):String;
  begin
    Result := IntToStr(n);
    while Length(Result)<5 do
    begin
      Result := '0' + Result;
    end;
  end;
var
  NumeroTest: Integer;
  Trovato: Boolean;
begin
  NumeroTest := 0;
  Trovato := False;

  While (NumeroTest<10000) and (not Trovato) do
  begin
    NomeFileLibero := GestioneConfigurazione.DammiCartellaCorrente +
           '\immagine_' + Suffisso(NumeroTest) + '.jpg';
    Trovato := not FileExists(NomeFileLibero);
    NumeroTest:=NumeroTest + 1;
  end;

  if NumeroTest>=10000 then
  begin
    NomeFileLibero:='';
  end;
end;

end.

